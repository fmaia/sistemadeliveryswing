package br.edu.ifce.poo.delivery.model;

public class ItemPedido {
	private Item item;
	private int quantidade;
	private String obs;
	public ItemPedido() {
	}
	public ItemPedido(Item item, int quantidade,
			String obs) {
		this.item = item;
		this.quantidade = quantidade;
		this.obs = obs;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	
	
}
