package br.edu.ifce.poo.delivery.model;

import java.util.List;

public class Pedido {
	private Cliente cliente;
	private List<ItemPedido> listaItensPedidos;
	private String status;
	public Pedido() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pedido(Cliente cliente, List<ItemPedido> listaItensPedidos, String status) {
		super();
		this.cliente = cliente;
		this.listaItensPedidos = listaItensPedidos;
		this.status = status;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<ItemPedido> getListaItensPedidos() {
		return listaItensPedidos;
	}
	public void setListaItensPedidos(List<ItemPedido> listaItensPedidos) {
		this.listaItensPedidos = listaItensPedidos;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public double calcValor() {
		double total = 0.0;
		int tamanho =this.listaItensPedidos.size();
		for(int i = 0; i < tamanho; i++) {
			double preco = 
					this.listaItensPedidos.get(i).
						getItem().getPreco();
			int quantidade = 
					this.listaItensPedidos.get(i).
						getQuantidade();
			total = total + preco*quantidade;
		}
		return total;
	}
}





